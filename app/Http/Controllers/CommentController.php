<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Comment;
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Comment $comment)
    {
        $comments = $comment->orderBy('created_at', 'DESC')->get();
        return view('comment.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Comment $comment, CommentRequest $request)
    {$this->authorize('create',$comment);
        $com=$comment->create($request->all());
//        return redirect()->route('comment.index');
        $response = array(
            'status' => 'success',
            'created_by' => $com->created_by,
            'comment' => $com->comment,
            'created_at' => $com->created_at,
            'com' => $com,
        );
        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Comment $comment, CommentRequest $request)
    {
        $u = $comment->where('id', $request->id)->first();
        $u->update($request->all());

        $response = array(
            'status' => 'success',
            'msg' => $u,
        );
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment, Request $request)
    {

        $u = $comment->where('id', $request->id)->first();
        $u->delete();

        $response = array(
            'status' => 'success',
            'msg' => $request->id,
        );
        return response()->json($response);
    }
}
