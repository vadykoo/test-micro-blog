<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use App\Gallery;
use Illuminate\Support\Facades\DB;
class PostController extends Controller
{
    protected $comment;
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $posts = $post->orderBy('created_at', 'DESC')->get();
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Post $post, PostRequest $request)
    {
        $id = $post->create($request->all())->id;
        if($files=$request->file('photos')){
            $i=0;
            foreach($files as $file){
                $photoName = time().$i++.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('post_photo'), $photoName);
                $photos[]=$photoName;
                DB::table('galleries')->insert(['post_id' => $id, 'photo' => $photoName]);
            }
        }

        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $photos = DB::table('galleries')->where(['post_id' => $post->id])->get();
        $comments = $this->comment->where([
            ['post_id', '=', $post->id],
            ['reference_id', '=', '0'],
        ])->get();
        $ref_comments = $this->comment->where([
            ['post_id', '=', $post->id],
            ['reference_id', '>', '0'],
        ])->get();
        return view('post.show', compact('post'),  compact('photos'))->with(compact('comments'))->with(compact('ref_comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {$this->authorize('update',$post);
        return view('post.edit' , compact('post') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Post $post, PostRequest $request)
    {$this->authorize('update',$post);
            if($files=$request->file('photos')){
                DB::table('galleries')->where('post_id', $post->id)->delete();
                $i=0;
                foreach($files as $file){
                    $photoName = time().$i++.'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('post_photo'), $photoName);
                    $photos[]=$photoName;
                    DB::table('galleries')->insert(['post_id' => $post->id, 'photo' => $photoName]);
                }
            }
        $post->update($request->all());

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {$this->authorize('delete',$post);
        $post->delete();
        return redirect()->route('posts.index');
    }
}
