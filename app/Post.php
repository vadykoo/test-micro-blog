<?php

namespace App;

use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class Post extends Model
{
    use HasTimestamps;
    protected $fillable = ['id', 'title', 'text', 'created_by', 'updated_by' ];
//    protected $appends = array('photos');
    protected $appends = ['photos', 'author'];

    public function getPhotosAttribute()
    {
        $post_id = $this->id;

            return $this->photos = DB::table('galleries')->where(['post_id' => $post_id])->value('photo');
    }
    public function getAuthorAttribute()
    {
        return $this->author = DB::table('users')->where(['id' => $this->created_by])->value('name');
    }
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id;
        });
        static::creating(function ($table) {
            $table->created_by = Auth::user()->id;
            $table->updated_by = Auth::user()->id;
        });


    }

    public function post(){
        return $this->hasMany(Gallery::class);
    }
}
