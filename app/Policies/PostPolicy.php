<?php

namespace App\Policies;

use App\User;
use App\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function view(User $user, Post $post)
    {
        return true;
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        switch ($user->role) {
            case 'admin':
            case 'editor':
                return true;
                break;
        }
        return false;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function update(User $user, Post $post)
    {
        switch ($user->role) {
            case 'admin':
                return true;
                break;
            case 'editor':
                if($user->id == $post->created_by){
                return true;
                break;
                }
                else {
                    return false;
                    break;
                }
        }
        return false;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function delete(User $user, Post $post)
    {
        switch ($user->role) {
            case 'admin':
                return true;
                break;
            case 'editor':
                if($user->id == $post->created_by){
                    return true;
                    break;
                }
                else {
                    return false;
                    break;
                }
        }
        return false;
    }
}
