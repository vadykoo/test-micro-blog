<?php

namespace App\Policies;

use App\User;
use App\Comment;
use Illuminate\Auth\Access\HandlesAuthorization;


class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the comment.
     *
     * @param  \App\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function view()
    {
        return true;
    }

    /**
     * Determine whether the user can create comments.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        switch ($user->role) {
            case 'admin':
            case 'editor':
            case 'user':
                    return true;
                    break;

        }
        return false;
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param  \App\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function update(User $user, Comment $comment)
    {
        switch ($user->role) {
            case 'admin':
                return true;
                break;
            case 'editor':
                if($user->id == $comment->created_by){
                    return true;
                    break;
                }
                break;
            case 'user':
                if($user->id == $comment->created_by){
                    return true;
                    break;
                }
                break;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param  \App\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        switch ($user->role) {
            case 'admin':
                return true;
                break;
            case 'editor':
                if($user->id == $comment->created_by){
                    return true;
                    break;
                }
                break;
            case 'user':
                if($user->id == $comment->created_by){
                    return true;
                    break;
                }
                break;
        }
        return false;
    }
}
