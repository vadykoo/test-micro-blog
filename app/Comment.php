<?php

namespace App;

use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class Comment extends Model
{
    use HasTimestamps;
    protected $fillable = ['post_id', 'comment', 'created_by', 'updated_by', 'reference_id' ];
    protected $appends = ['author'];
    public function getAuthorAttribute()
    {
        return $this->author = DB::table('users')->where(['id' => $this->created_by])->value('name');
    }
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id;
        });
        static::creating(function ($table) {
            $table->created_by = Auth::user()->id;
            $table->updated_by = Auth::user()->id;
        });


    }
}
