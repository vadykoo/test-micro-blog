@extends('layouts.panel')
@section('panel')
    <div class="panel-heading container-fluid">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        @if (Auth::user() and Auth::user()->role_id == 2)
                            <a class = 'btn btn-outline-dark btn-xs btn-block' href={{ route('users.index')}}>Show Users List</a>
                        @endif
                        @if (Auth::user() and (Auth::user()->role_id == 2 or Auth::user()->role_id == 1))
                        <a class = 'btn btn-info btn-xs btn-block' href={{ route('posts.create')}}>Create NEW POST</a>
                        @endif

                    @foreach ($posts as $post)
                        <div class="card mb-4">
                            @if ($post->photos != NULL)
                            <img class="img-fluid rounded" src="{{asset('/post_photo/'.$post->photos)}}" alt="Card image cap">
                            @endif
                            <div class="card-body">
                                <h2 class="card-title">{{$post->title}}</h2>
                                <p class="card-text">{{ str_limit($post->text, $limit = 25, $end = '...') }}</p>
                                {{--<a class = 'btn btn-danger btn-xs' href={{ route('posts.destroy', ['id' => $post->id])}}>delete</a>--}}
                                @if (Auth::user() and ($post->created_by == Auth::user()->id or Auth::user()->role_id == 2))
                                <form action="{{ route('posts.destroy', ['id' => $post->id]) }}" method="POST" style="display: inline-block; float: right">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button style="display: inline-block" class = 'btn btn-danger btn-xs'>Delete</button>
                                    <a class = 'btn btn-info btn-xs ' style="display: inline-block" href={{ route('posts.edit', ['id' => $post->id] )}}>Edit</a>
                                </form>
                                @endif
                                <a class="btn btn-primary btn" style="display: inline-block" href={{ route('posts.show', ['id' => $post->id])}} >Read More &rarr;</a>

                            </div>
                            <div class="card-footer text-muted">
                                {{$post->created_at}}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
    </div>

@endsection
