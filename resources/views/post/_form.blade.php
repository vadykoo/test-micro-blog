<div class="form-group ">
    <label for="title">Title:</label>
    <input type="text" class="form-control" name="title" placeholder="Enter title" value="{{$post->title or NULL}}">
</div>
<div class="form-group ">
    <label for="text">Text:</label>
    <input type="text" class="form-control" name="text" placeholder="Text" value="{{$post->text or NULL}}">
</div>
<div class="form-group ">
    {{--<input type="file" name="photo">--}}
    Вы можете выбрать сразу несколько фотографий, для этого выделите их в окне выбора.
    <input type="file" class="form-control" name="photos[]" placeholder="address" multiple>

</div>
