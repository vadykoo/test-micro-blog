@extends('layouts.panel')
@section('panel')
    <div class="panel-heading container-fluid">
        @if (Auth::user() and ($post->created_by == Auth::user()->id or Auth::user()->role_id == 2))
            <form action="{{ route('posts.destroy', ['id' => $post->id]) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button class = 'btn btn-danger btn-xs'>Delete</button>
                <a class = 'btn btn-info btn-xs ' href={{ route('posts.edit', ['id' => $post->id] )}}>Edit</a>
            </form>
        @endif
    </div>

    <div class="container">

        <div class="row">

            <div class="col-lg-10">

                <h1 class="mt-4">{{$post->title}}</h1>

                <p class="lead">
                    by
                    <a href="#">{{$post->author}}</a>
                </p>

                <hr>

                <p>{{$post->created_at}}</p>

                <hr>


                <hr>

                <p>{{$post->text}}</p>

                <hr>
                @if ($photos != NULL)
            @foreach ($photos as $photo)
                    <img class="img-fluid rounded" src="{{asset('/post_photo/'.$photo->photo)}}" alt="{{$photo->photo}}">
            @endforeach
            @endif
            @include('comment.create');
                @include('comment.index', ['comments' => $comments]);

            </div>

@endsection