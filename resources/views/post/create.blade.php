@extends('layouts.panel')
@section('panel')
    <div class="panel-heading container-fluid">
        {{--<div class="form-group">--}}
            {{--<div class="centered-child col-md-11 col-sm-10 col-xs-10"><b>New Post</b></div>--}}
        {{--</div>--}}
    </div>

    <div class="panel-body">

    <form method="post" action="{{action('PostController@store')}}" enctype="multipart/form-data">
        @csrf
        @include('post._form')
            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
    </form>
    </div>

@endsection