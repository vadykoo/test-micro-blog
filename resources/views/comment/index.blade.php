<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function edit(comment){
            $.ajax({
                url: '/comment-delete',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:number},
                dataType: 'JSON',
                success: function (data) {
                    $("#"+number).remove();
                }
            });
        }
        function del(number){
            $.ajax({
                url: '/comment-delete',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:number},
                dataType: 'JSON',
                success: function (data) {
                    $("#"+number).remove();
                }
            });
        }
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".deletebutton").click(function del() {
                var number = $(this).val();
                $.ajax({
                    url: '/comment-delete',
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: number},
                    dataType: 'JSON',
                    success: function (data) {
                        $("#" + number).remove();
                    }
                });
            });
        });
            function edit_comment( id ) {
                var comment_text = $("#comment_text_"+id).val();
                console.log(comment_text);
                $.ajax({
                    url: '/comment-update',
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: id, comment: comment_text},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data.msg);
                        $("#comment_"+ id).html('<p id="comment_'+id+'">'+ data.msg.comment +'</p> ');
                    }
                });
            }
        $(document).ready(function() {
            $(".editbutton").click(function(){
                var comment = JSON.parse($(this).val());
            console.log(comment);
                $("#comment_"+ comment.id).html(
                            '<div class="form-group" id="comment_'+comment.id+'">' +
                        '<input class="form-comment"  id="comment_text_'+comment.id+'"  name="comment_text" value="'+comment.comment+'">'
                      +  '<button type="submit" onClick="return edit_comment('+comment.id+');" class="postbutton btn btn-success">Submit</button>' +
                        '</div>'
                        );
            });
        });
        function show_edit_line (id){
            var comment_text = $("#comment_text_n"+id).val();
            console.log(comment_text);
            $("#comment_text_new_"+ id).html(
                '<div class="form-group" id="comment_'+id+'">' +
                '<input class="form-comment"  id="comment_text_'+id+'"  name="comment_text" value="'+comment_text+'">'
                +  '<button type="submit" onClick="return edit_comment('+id+');" class="postbutton btn btn-success">Submit</button>' +
                '</div>'
            );
        }
        $(document).ready(function(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".postbutton_ans").click(function(){
                var id = this.id;
                console.log(id);
                $.ajax({
                    url: '/comment',
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, post_id:$(".form-post-"+id).val(), comment:$(".form-comment-"+id).val(), reference_id:id },
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data.com.id);
                        $("#com-"+id).append(
                            '<div class="media mb-4"  style="margin-left: 50px;" id="'+data.com.id+'">' +
                            '    <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">' +
                            '    <div class="media-body">' +
                            '        <h5 class="mt-0">'+data.com.author+'</h5>' +
                            ' <input   id="comment_text_n'+data.com.id+'" type="hidden" value='+data.com.comment+'>' +
                            ' <p id="comment_text_new_'+data.com.id+'" >'+data.com.comment+'</p> '+
                            '    </div>' +
                            ' <button type="submit" class="editbutton btn btn-success" onClick="return show_edit_line('+data.com.id+');">Edit</button>'+
                            '  <button type="submit" class="deletebutton btn btn-danger" onClick="return del('+data.com.id+');" value='+data.com.id+'>Delete</button>'+
                            '</div>');$(".form-comment-"+id).val('');
                    }
                });
            });
        });
    </script>

@foreach($comments as $comment)
<div class="media mb-4" id="{{$comment->id}}">
    <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
    <div class="media-body">
        <h5 class="mt-0">{{$comment->author}}</h5>
        <p id="comment_{{$comment->id}}">{{$comment->comment}} </p>   </div>
    @if (Auth::user() and ($comment->created_by == Auth::user()->id or Auth::user()->role_id == 2))

    <button type="submit" class="editbutton btn btn-success"  value="{{$comment}}">Edit</button>
    <button type="submit" class="deletebutton btn btn-danger" value="{{$comment->id}}">Delete</button>
@endif
</div>
@if(isset($ref_comments))
            @foreach($ref_comments as $ref_comment)
                @if($ref_comment->reference_id == $comment->id)
                <div class="media mb-4" style="margin-left: 50px;" id="{{$ref_comment->id}}">
                    <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                    <div class="media-body">
                        <h5 class="mt-0">{{$ref_comment->author}}</h5>
                        <p id="comment_{{$ref_comment->id}}">{{$ref_comment->comment}} </p>  </div>
                    @if (Auth::user() and ($ref_comment->created_by == Auth::user()->id or Auth::user()->role_id == 2))

                        <button type="submit" class="editbutton btn btn-success"  value="{{$ref_comment}}">Edit</button>
                        <button type="submit" class="deletebutton btn btn-danger" value="{{$ref_comment->id}}">Delete</button>
                    @endif
                </div>
                @endif

            @endforeach
            @endif
<div id="com-{{$comment->id}}"></div>

@if (Auth::user())

<div class="form-group-{{$comment->id}}" style="margin-left: 120px; margin-bottom: 40px">
    <input class="form-post-{{$comment->id}}"  id="post_id" name="post_id" type="hidden" value="{{$post->id}}">
    {{--<input class="form-add"  id="reference_id" name="reference_id" type="hidden" value="{{$comment->id}}">--}}
    <p>Reply to comment:</p>
    <input style="width: 50%;" class="form-comment-{{$comment->id}}"  name="comment">
    <button type="submit"  class="postbutton_ans btn btn-success" id='{{$comment->id}}'>Send</button>
</div>
@endif
    @endforeach
<div class="writeinfo"></div>
