<!-- load jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- provide the csrf token -->
<meta name="csrf-token" content="{{ csrf_token() }}" />

<script>
    $(document).ready(function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(".postbutton").click(function(){
            $.ajax({
                url: '/comment',
                type: 'POST',
                data: {_token: CSRF_TOKEN, post_id:$(".form-post").val(), comment:$(".form-comment").val(), reference_id:$(".form-add").val(), },
                dataType: 'JSON',
                success: function (data) {
                    console.log(data.com.id);
                    $(".writeinfo").append('<div class="media mb-4" id="'+data.com.id+'">' +
                        '    <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">' +
                        '    <div class="media-body">' +
                        '        <h5 class="mt-0">'+data.com.author+'</h5>' +
                        ' <input   id="comment_text_n'+data.com.id+'" type="hidden" value='+data.com.comment+'>' +
                        ' <p id="comment_text_new_'+data.com.id+'" >'+data.com.comment+'</p> '+
                        '    </div>' +
                        ' <button type="submit" class="editbutton btn btn-success" onClick="return show_edit_line('+data.com.id+');">Edit</button>'+
                      '  <button type="submit" class="deletebutton btn btn-danger" onClick="return del('+data.com.id+');" value='+data.com.id+'>Delete</button>'+
                        '</div>');                    $(".form-comment").val('');

                }
            });
        });
    });

</script>
@if (Auth::user())
<div class="card my-4">
    <h5 class="card-header">Leave a Comment:</h5>
    <div class="card-body">
            @include('comment._form')
                <button type="submit" class="postbutton btn btn-success">Submit</button>
    </div>
</div>
@endif
