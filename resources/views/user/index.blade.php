@extends('layouts.panel')
@section('panel')
    <!-- load jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- provide the csrf token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script>
        $(document).ready(function(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".editbutton").click(function(){
                var number = $(this).val();
                $.ajax({
                    /* the route pointing to the post function */
                    url: '/users-update',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, id:number, name:$("#"+number+".form-name").val(), email:$("#"+number+".form-email").val(), role_id:$("#"+number+".form-role_id").val()},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                        $(".writeinfo").append(data.status);
                        console.log(data);
                    }
                });
            });
        });

        $(document).ready(function(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".deletebutton").click(function(){
                var number = $(this).val();
                $.ajax({
                    /* the route pointing to the post function */
                    url: '/users-delete',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, id:$(this).val()},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                        $("#"+number).remove();
                        console.log(data);
                    }
                });
            });
        });
    </script>
    <div class="panel-heading container-fluid">
            <div class="container">
                Role_id
                (0-User, 1-Editor, 2-admin)
                <div class="row">
                        <table class="table table-bordered table-responsive table-striped table-hover ">
                            <tr class="">
                                <th width="10%">id</th>
                                <th width="25%">Name</th>
                                <th width="30%">email</th>
                                <th width="10%">Role_id</th>
                                <th width="25%">Actions</th>

                            </tr>
                            {{--<tr>--}}
                            {{--<td colspan="3" class="light-green-background no-padding" title="Create new template">--}}
                            {{--<div class="row centered-child">--}}
                            {{--<div class="col-md-12">--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            @foreach ($users as $user)
                                    <tr class="table-row" style="cursor: pointer" id="{{$user->id}}">
                                    <td>{{$user->id}}</td>
                                        {{--<input type="hidden" class="id_user" name="id_user" value="{{$user->id}}">--}}
                                        <td><input type="text" class="form-name" name="name" id="{{$user->id}}" value="{{$user->name}}"></td>
                                        <td><input type="text" class="form-email" name="email" id="{{$user->id}}" value="{{$user->email}}"></td>
                                        <td><input type="text" class="form-role_id" name="role_id" id="{{$user->id}}" value="{{$user->role_id}}"></td>
                                        <td>
                                        {{--{{Form::open(['class' => 'confirm-delete', 'route' => ['subscribers.destroy',  $model->bunches_id, $model->id],--}}
                                       {{--'method' => 'DELETE'])}}--}}
                                        {{--{{ link_to_route('subscribers.show', 'info', [ $model->bunches_id, $model->id], ['class' => 'btn btn-info btn-xs']) }}--}}
                                        {{--{{ link_to_route('subscribers.edit', 'edit', [ $model->bunches_id, $model->id], ['class' => 'btn btn-success btn-xs']) }}--}}
                                        {{--{{Form::button('Delete', ['class' => 'btn btn-danger btn-xs', 'type' => 'submit'])}}--}}
                                        {{--{{Form::close()}}--}}
                                            <button type="submit" class="editbutton btn btn-success" value="{{$user->id}}">Save</button>
                                            <button type="submit" class="deletebutton btn btn-danger" value="{{$user->id}}">Delete</button>

                                        </td>
                                </tr>
                        @endforeach
                    </div>
                </div>
            </div>
    </div>

@endsection
