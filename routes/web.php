<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::resource('/', 'BunchController');

Auth::routes();
Route::get('/', 'PostController@index');
Route::resource('/posts', 'PostController');
Route::resource('/comment', 'CommentController');
Route::post('/comment-update', 'CommentController@update');

Route::post('/comment-delete', 'CommentController@destroy');
Route::resource('/users', 'UserController');
Route::post('/users-update', 'UserController@update');
Route::post('/users-delete', 'UserController@destroy');

//Route::get('/', 'PostController@index');
//Route::get('/post/create', 'PostController@create');
//Route::post('/store', 'PostController@store');
//Route::get('/show{id}', 'PostController@show');
//Route::get('/home', 'HomeController@index')->name('home');
//Route::post('/store', 'PostController@store');
//Route::post('/', ['as' => 'form-group', 'uses' => 'PostController@store']);
//Route::post('/', ['as' => 'form_url', 'uses' => 'PostController@store']);